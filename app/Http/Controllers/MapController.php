<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapController extends Controller
{

    public function getPlaces(Request $request){
        $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$request['query'].'&radius=1500&type=bank&keyword=BPI&key=AIzaSyAlOKImiqwuNcZCx9u3x9YjHztzV8CM9p8';
          
          $ch = curl_init($url);

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, 0);

          $result = curl_exec($ch);
          curl_close($ch);
          return $result;
    }
    
}
