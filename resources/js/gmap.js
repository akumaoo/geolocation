var infoWindow,map;
    function initMap(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        map = new google.maps.Map(document.getElementById('map'), {
            center:pos,
            zoom: 18,
            clickableIcons:false,
        });

        var mymarker=new google.maps.Marker({
        map: map,
        position: pos,
        icon:'https://img.icons8.com/dusk/50/000000/street-view.png',
        title:'Current Position', 
        });


        var curpos = new google.maps.LatLng(pos);
        
        infowindow = new google.maps.InfoWindow();

        var myclick=google.maps.event.addListener(mymarker, 'click', function() {
            var content="<strong>My Current Position</strong>";
        infowindow.setContent(content);
        infowindow.open(map,mymarker);
        });


        var requests = [
            {
            location: curpos,
            radius: '500',
            type:['bank'],
            // name:['Unionbank*'],
            },
            {
            location: curpos,
            radius: '500',
            // type:['bank'],
            name:['7-Eleven'],
            },
            
        ];

        service = new google.maps.places.PlacesService(map);
        requests.forEach(gmap_setup)

        function gmap_setup(request,index)
        {
            service.nearbySearch(request, callback);
        }
        
    
        }, function(error) {
        //   Enable location
            if(error.code==1)
            {
                alert('Message');
            }
            else{
            handleLocationError(true, infoWindow, map.getCenter());
            }
        });

    }
    else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
    }

    function createMarker(place) {
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location 
    });

    google.maps.event.addListener(marker, 'click', function() {
        var content="<strong>"+place.name+"</strong><br><em>Open: "+place.opening_hours.open_now+"<em><br>";
        infowindow.setContent(content);
        infowindow.open(map, this);
    });
    }

    function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
        var place = results[i];
        createMarker(place);
        }
    }
    }    
}

