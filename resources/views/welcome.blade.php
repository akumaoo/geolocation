<!DOCTYPE html>
<html>
    <head>
        <title>Simple Map</title>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <style>
            #map{
            height: 400px;
            width: 550px;
            margin:auto;
            }
            .modal-footer>.info-panel{
            text-align:left;
            margin-left:10px;
            }
            .modal-footer>.info-panel>p{
            margin-bottom: 0px;
            font-size: 13px;
            }
            .modal-footer{
            overflow:auto;
            max-height: 200px;
            text-align:center;
            }
            .controls{
                margin-top:200px;
                margin-left:43%
            }
            .controls>a{
                width:100px;
                border-color: #6fbedd;
            }
            .controls>a:hover{
                border-color:whitesmoke;
                color:whitesmoke;
                background-color: #6fbedd;
            }
        </style>
    </head>
    <body>
        <div class="controls">
        <a href="javascript:void(0)" data-type="map" class="click btn">Maps</a>
        <a href="javascript:void(0)" data-type="list" class="click btn">List</a>
        </div>
        
        <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-body">
                        <div id="map">
                            <div class="container-fluid"> 
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{-- <a href="javascript:void(0)" id="all_list" data-id="All" class="btn btn-info ">All Banks</a> --}}
                        <a href="javascript:void(0)" id="ub_list" data-id="UB" class="btn btn-info">UnionBank</a>
                        <a href="javascript:void(0)" id="bpi_list" data-id="bpi" class="btn btn-info">BPI</a>
                        <a href="javascript:void(0)" id="mb_list" data-id="metrobank" class="btn btn-info">Metrobank</a>
                    </div>
                    </div>
                </div>
        </div>

        <div id="myModalList" class="modal fade" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-body">
                        <div>
                            <div class="container-fluid">
                                <div class="row">
                                        <div class="col-lg-12">
                                            <h4 style="text-align:center"><strong>Banks</strong></h4>
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="container-fluid" id="bank-lists">
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{-- <a href="javascript:void(0)" id="all" data-id="All" class="btn btn-info ">All Banks</a> --}}
                        <a href="javascript:void(0)" id="ub" data-id="UB" class="btn btn-info ">UnionBank</a>
                        <a href="javascript:void(0)" id="bpi" data-id="bpi" class="btn btn-info">BPI</a>
                        <a href="javascript:void(0)" id="mb" data-id="metrobank" class="btn btn-info">Metrobank</a>
                    </div>
                    </div>
                    </div>
                
                </div>
           
       <script type="text/javascript">
       $(function(){
        //    CONTROLLERS
            $('.click').click(function(){
                initMap('UB');
                if($(this).attr("data-type")=="map"){     
                    $('#myModal').modal('show');
                }else{
                    $('#myModalList').modal('show');
                }
            });

            $('.btn').click(function(){
                // var id=$('.active').attr('id');
                // $('#'+id).removeClass('active');
                // $(this).addClass('active');
                
                initMap($(this).attr('data-id'));
            });
        

            var infoWindow,map,pos,curpos;
            function initMap(selected){
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                    pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    }; 
                    // Load nearby places request
                    loadMaps(selected,pos);
                    
     
                    }, function(error) {    
                    //location not enabled        
                        if(error.code==1){
                            $('#map').html('<img src="https://cdn4.iconfinder.com/data/icons/mobile-icon-2/512/denied2-512.png">');
                        }
                    });
                }else{
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
            }

            async function loadMaps(selected,pos){
                map = new google.maps.Map(document.getElementById('map'), {
                center:pos,
                zoom: 18,
                clickableIcons:false,
                streetViewControl:false,
                });

                // generate user's current location marker
                var mymarker=new google.maps.Marker({
                map: map,
                position: pos,
                icon:'https://img.icons8.com/dusk/50/000000/street-view.png',
                title:'Current Position', 
                });
            
                infowindow = new google.maps.InfoWindow();
                var myclick=google.maps.event.addListener(mymarker, 'click', function() {
                var content="<strong>My Current Position</strong>";
                    infowindow.setContent(content);
                    infowindow.open(map,mymarker);
                });

                curpos = new google.maps.LatLng(pos);

                // bank lists
                // radius measures in meters
                var requests = [
                    {
                    location: curpos,
                    radius: '5000',
                    keyword:["(BPI) OR (Bank of the Philippine Islands) OR (BPI Place Branch)"],
                    type:['bank'],
                    },{
                    location: curpos,
                    radius: '5000',
                    name:["Metrobank"],
                    type:['bank'],
                    },{
                    location: curpos,
                    radius: '5000',
                    type:['bank'],
                    keyword:["(Union Bank ATM) OR (Union Bank) OR (UnionBank) OR (UnionBank of the Philippines) OR (Union Bank of the Philippines)"],
                    }
                ];
                
                // select what bank to search base on the user input
                if(selected=='metrobank'){
                    $key=1;
                }else if(selected=='bpi'){
                    $key=0;
                }else if(selected=='UB'){
                    $key=2;
                }
                
                // generate place lists base on the user request
                service = new google.maps.places.PlacesService(map);
                service.nearbySearch(requests[$key],callback);    
            }

            function removeKmChar($string){
                var m=$string.slice(-2);
                if(m=='km'){
                    return parseFloat($string);
                }else{
                    var num=parseFloat($string);
                    return num/1000;                  
                }
            }

            function getTopThree($array){
                return new Promise(function(resolve,reject){
                var prep_array=[];
                for($n=0;$n<$array.length;$n++){
                    var current=removeKmChar($array[$n]);
 
                    prep_array.push({
                        key:$n,
                        val:current
                    });
                }
            
                prep_array.sort(function(a,b){
                    return a.val - b.val;
                });
                
                // console.log(prep_array);
                resolve([prep_array[0].key,prep_array[1].key,prep_array[2].key]);
                });
            }

            async function callback(results, status) {
                var plists=[],distance=[],dkey=[];
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    var place,marker,content,dpos,lists_content="";
                    
                    for (var i = 0; i < results.length; i++) {
                        place = results[i];
                        marker = new google.maps.Marker({
                        map: map,
                        animation: google.maps.Animation.DROP,
                        position: place.geometry.location 
                        });
                       
                        
                        fillMaps(place,marker);

                        dpos = {
                            lat: place.geometry.location.lat(),
                            lng: place.geometry.location.lng()
                        };
                        
                        distance.push(await fillLists(dpos,place));
                        plists.push(place);
                    }
                    // console.log(plists);
                    // console.log(distance);
                    // if the generated lists is not lower than 3 then sort them by closest bank
                    if(distance.length>=3){
                        var filtered_data=await getTopThree(distance);
                        // console.log(filtered_data);
                    }else{
                        var filtered_data=[];
                        for($inc=0;$inc<distance.length;$inc++){
                            filtered_data[$inc]=$inc;
                        }
                    }
                    
                    // draw content lists 
                    for($q=0;$q<filtered_data.length;$q++){
                        lists_content+='<div class="row"><div class="col-lg-12"><h4><strong>'+plists[filtered_data[$q]].name+'</strong></h4><p><em>Address:'+plists[filtered_data[$q]].vicinity+'</em></p><p><em>Distance:'+distance[filtered_data[$q]]+'</em></p></div></div>';
                    }
                    $('#bank-lists').html(lists_content);
                    
                   
                }else{
                    $('#bank-lists').html('No Banks Nearby');
                    console.log('Error in getting results:[Status]'+status);    
                }
            }

            function fillMaps(place,marker){
                google.maps.event.addListener(marker, 'click',function() {
                var content="<strong>"+place.name+"</strong><br>Address: "+place.vicinity;
                infowindow.setContent(content);
                infowindow.open(map, this);
                });
            }

            function fillLists(dpos,place){
                return new Promise(function(resolve,reject){
                var distanceService = new google.maps.DistanceMatrixService;
                distanceService.getDistanceMatrix({
                origins: [pos],
                destinations: [dpos],
                travelMode: 'DRIVING',
                },function (response, status) {
                    if (status != google.maps.DistanceMatrixStatus.OK) {
                        console.log('Error:', status);
                    } else {
                        resolve(response.rows[0].elements[0].distance.text);
                    }
                });
                
            });
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                    'Error: The Geolocation service failed.' :
                                    'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
            }
        });
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlOKImiqwuNcZCx9u3x9YjHztzV8CM9p8&callback=initMap&libraries=places" async defer></script>        

    </body>
</html>